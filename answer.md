• Q1: Define what is a Microservices architecture and how does it differ
from a Monolithic application.

    Microservices is a type of architecture in which a software application is composed of small, independent services that communicate together using APIs.
    In a monolithic application the entire application is built as a single unit. All components, functions, and services are interdependent.
    Microservices are differents because they break down the application into small, independent services, each responsible for a specific capability. Each microservice can be developed, deployed, and scaled independently so it's the opposite of monolithic


• Q2: Compare Microservices and Monolithic architectures by listing their
respective pros and cons.

    Microservices:
        Pro :
            - Scalability: can scale individual services independently
            - Agility: easy to deploy, update and maintain
            - Technology diversity: each microservice can use different technologies
            - Error handling: failures in one service don't necessarily affect others
        Cons:
            - Complexity: understanding the architecture is more complex
            - Communication: services need to communicate over a network which can be hard to set

    Monolithic:
        Pros:
            - Simplicity: easy to develop and test due to a single codebase
            - Performance: tight integration can lead to better performance
            - Development simplicity: Fewer components to manage
        Cons:
            - Scalability: scaling requires replicating the entire application
            - Flexibility: you have to use same are similar technologies for the whole app
            - Maintenance: updates require all app to be down


• Q3: In a Micoservices architecture, explain how should the application be
split and why.

    In microservices the app has to be splitted based on business capabilities, each microservice should represent a specific business function in order to differenciate things that doesn't rely on others and which uses different technologies.
    Moreover  you have to apply a decentralized data management, each microservice should have its own database, reducing dependencies.
    Then you have to define clear interfaces by using APIs for communication between microservices.
    

• Q4: Briefly explain the CAP theorem and its relevance to distributed
systems and Microservices.

    The CAP theorem, proposed by Eric Brewer, states that in a distributed system, it is impossible to simultaneously provide all three of the following guarantees: Consistency, Availability, and Partition Tolerance.
    Consistency means that all clients see the same data at the same time, no matter the path of their request. This is critical for applications that do frequent updates.

    Availability means that all functioning application components will return a valid response, even if they are down. This is particularly important if an application's user population has a low tolerance for outages.

    Partition tolerance means that the application will operate even during a network failure that results in lost or delayed messages between services. This comes into play for applications that integrate with a large number of distributed, independent components.

    Microservices often operate in distributed environments, making trade-offs between consistency and availability during network partitions.
    Design choices in microservices architectures are influenced by the need for partition tolerance and the trade-off between consistency and availability.


• Q5: What consequences on the architecture ?

    Microservices architectures need to carefully consider data consistency and communication patterns.
    Trade-offs between strong consistency and high availability need to be made based on specific application requirements.
    So fault tolerance and resilience becomes crucial.


• Q6: Provide an example of how microservices can enhance scalability in
a cloud environment.

    In a microservices architecture deployed in the cloud individual services can be scaled independently based on demand.
    For instance a service handling user authentication can be scaled separately from a service handling file storage allowing efficient resource allocation based on the specific needs of each service.
    So it gives the developper to upgrade a part of the cloud application withouth affecting other services.


• Q7: What is statelessness and why is it important in microservices architecture ?

    Statelessness in microservices means that each service does not store information about the state of the system, so services treat each request independently and don't rely on stored session information.
     It is important in microservices because statelessness simplifies scaling as any instance of a service can handle any requestand it also improves fault tolerance and resilience as there is no shared state that can be lost in case of a service failure.


• Q8: What purposes does an API Gateway serve ?

    An API Gateway is a server that acts as an API front-end by receiving API requests enforcing throttling and security policies passing requests to the back-end services and then passing the response back to the requester.
    So it desserves purposes of routing by directing the client request, but also authentication to handle the authorization, it acts for load balancing as it distributes incoming request on multiples services instances and it can also be used for aggregation as it can merge multiple request into a single one

