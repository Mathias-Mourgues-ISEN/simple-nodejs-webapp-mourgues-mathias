FROM node:latest

WORKDIR /usr/src/app

COPY . .

EXPOSE 8080

ENV APPLICATION_INSTANCE example


CMD ["node", "src/count-server.js"]
